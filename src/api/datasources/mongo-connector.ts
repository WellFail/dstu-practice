import { MongoClient } from 'mongodb';

const url = process.env.MONGO_URL;

const client = new MongoClient(url);

const connect = async () => {
  try {
    return await client.connect();
  } catch (e) {
  }
};

const getDatabaseConnection = async () => await connect();

export default getDatabaseConnection;
