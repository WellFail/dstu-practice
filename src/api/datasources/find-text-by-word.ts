import getDatabaseConnection from './mongo-connector';

const dbName = process.env.MONGO_DB_NAME;

const findText = async ({ word }) => {
  const connection = await getDatabaseConnection();
  const db = await connection.db(dbName);
  const collection = db.collection('documents');

  const res = await collection.find({ $text: { $search: word } }).toArray();
  await connection.close();

  return res;
};

export default findText;
