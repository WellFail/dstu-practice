import './startup';
import './fixtures';

import server from './server';

const port = process.env.PORT;

server.listen(port, () => {
  console.log(`Server started on http://localhost:${port}`);
});
