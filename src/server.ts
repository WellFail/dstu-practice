import express from 'express';

import findTextByWord from './api/datasources/find-text-by-word';

const server = express();

server.get('/', async (request, response) => {
  const { word } = request.query;
  if (!word) return response.send('Find text by your word');

  const result = await findTextByWord({ word });

  if (result.length > 0) return response.send(result);

  return response.send('Word not found');
});

export default server;
