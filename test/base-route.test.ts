import chai from 'chai';
import chaiHttp from 'chai-http';

import app from '../src/server';

chai.use(chaiHttp);
const expect = chai.expect;
describe('baseRoute', () => {
  it('should respond with "Find text by your word" text', () => {
    return chai.request(app)
          .get('/')
          .then((res) => {
            expect(res.text).to.be.equal('Find text by your word');
          });
  });
});
